package edu.missouristate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootshoppeApplication {

	public static void main(String[] args) {
		SpringApplication.run(BootshoppeApplication.class, args);
	}

}
