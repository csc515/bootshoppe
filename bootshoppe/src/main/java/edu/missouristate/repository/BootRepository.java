package edu.missouristate.repository;

import org.springframework.data.repository.CrudRepository;

import edu.missouristate.domain.Boot;

public interface BootRepository extends CrudRepository<Boot, Integer>, BootRepositoryCustom {
	// Spring Data abstract methods go here
	
	
}
