package edu.missouristate.repository;

import java.util.List;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import edu.missouristate.domain.Boot;
import edu.missouristate.domain.QBoot;

@Repository
public class BootRepositoryImpl extends QuerydslRepositorySupport implements BootRepositoryCustom {
	
	QBoot bootTable = QBoot.boot;
	
	public BootRepositoryImpl() {
		super(Boot.class);
	}

	// Object Oriented SQL goes here...
	public Boot getBootByColor(String color) {
		return (Boot) from(bootTable)
				.where(bootTable.color.eq(color))
				.orderBy(bootTable.id.asc())
				.fetch();
	}
	
	public List<Boot> getBootListByBrand(String brand) {
		return from(bootTable)
				.where(bootTable.brand.eq(brand))
				.orderBy(bootTable.id.asc())
				.fetch();
	}
	
}
