package edu.missouristate.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.missouristate.domain.Boot;
import edu.missouristate.repository.BootRepository;

@Service("bootService")
public class BootService {

	@Autowired
	BootRepository bootRepo;
	
	public List<Boot> getInventory() {
		return (List<Boot>) bootRepo.findAll();
	}

	@Transactional
	public void addBoot(Boot boot) {
		// TODO Auto-generated method stub
		// add boot using CRUD library!
		bootRepo.save(boot);
	}
	
	@Transactional
	public void editBoot(Boot boot) {
		// TODO Auto-generated method stub
		// add boot using CRUD library!
		bootRepo.save(boot);
	}
	
	@Transactional
	public void deleteBoot(Boot boot) {
		// TODO Auto-generated method stub
		bootRepo.delete(boot);
	}
	
}
