package edu.missouristate.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import edu.missouristate.domain.Boot;
import edu.missouristate.service.BootService;

@Controller
public class BootController {

	@Autowired
	BootService bootService;
	
	@GetMapping(value="/")
	public String getIndex(Model model) {
		List<Boot> bootList = bootService.getInventory();
		model.addAttribute("bootList", bootList);
		return "index";
	}
	
	@GetMapping(value="/editBoot")
	public String getEditBoot(Model model, Integer id) {
		model.addAttribute("action", "Edit");
		model.addAttribute("url", "/editBoot");
		//Boot boot = bootService.getBootById(id);
		// ...
		return "addBoot";
	}

	@PostMapping(value="/editBoot")
	public String postEditBoot(Model model, @RequestBody Boot boot) {
		// add the new boot to inventory
		//bootService.editBoot(boot);
		return "redirect:/";
	}
	
	@GetMapping(value="/addBoot")
	public String getAddBoot(Model model) {
		model.addAttribute("action", "Add");
		model.addAttribute("url", "/addBoot");
		return "addBoot";
	}
	
	@PostMapping(value="/addBoot")
	public String postAddBoot(Model model, @RequestBody Boot boot) {
		// add the new boot to inventory
		bootService.addBoot(boot);
		return "redirect:/";
	}
	
	@GetMapping(value="/sw.js")
	public String getSw(Model model) {
		return "";
	}
	
}
