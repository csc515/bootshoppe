package edu.missouristate.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "inventory")
public class Boot {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id; // `id` INT NOT NULL AUTO_INCREMENT,

	@Column
	private String brand; // `brand` VARCHAR(45) NOT NULL,

	@Column
	private String bootYear; // `boot_year` VARCHAR(45) NOT NULL,

	@Column
	private String color; // `color` VARCHAR(45) NOT NULL,

	@Column
	private String size; // `size` VARCHAR(45) NOT NULL,

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getBootYear() {
		return bootYear;
	}

	public void setBootYear(String bootYear) {
		this.bootYear = bootYear;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

}
