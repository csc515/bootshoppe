<%@ include file="/WEB-INF/views/includes/include.jsp" %>
<!doctype html>
<html lang="en">
<head>
	<title>BootShop</title>
	<!-- Bootstrap 4.5 CSS-->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">	
	<!-- Bootstrap JS Requirements -->
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1>Dada's Bootshop Inventory Manager</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<a href="<%=request.getContextPath() %>/addBoot" class="btn btn-primary" >Add Boot</a>
				<table class="table table-hover table-striped table-bordered mt-2">
					<tr>
						<th>ID</th>
						<th>Brand</th>
						<th>Year</th>
						<th>Color</th>
						<th>Size</th>
						<!-- add city state zip -->
					</tr>
					<!-- 
						* Insert Data into your table (using add button) and then
						* Build a table here and loop through the data and display it	
					-->
					<!-- Handle when no data found -->
					<c:if test="${empty bootList}">
						<tr><td colspan="5">No Records Found</td></tr>
					</c:if>
					<!-- Loop Here -->
					<c:if test="${not empty bootList}">
						<!-- 
							 Add <tr> elements and <td> elements below inside 
							 of the c:forEach tags (build the body of the table)
						-->
						<c:forEach var="boot" items="${bootList}" > 
							<tr><td colspan="5">${boot.id}</td></tr>
							<!-- 
								${boot.brand}, ....
							 -->
						</c:forEach>
					</c:if>
				</table>
			
			</div>
		</div>
	</div>
</body>
</html>
