CREATE database bootshop;

CREATE TABLE `bootshop`.`inventory` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `brand` VARCHAR(45) NOT NULL,
  `boot_year` VARCHAR(45) NOT NULL,
  `color` VARCHAR(45) NOT NULL,
  `size` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));
